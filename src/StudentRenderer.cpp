#include "StudentRenderer.hpp"

StudentRenderer::StudentRenderer()
{
}

bool StudentRenderer::init(std::shared_ptr<Scene> scene, unsigned int screenWidth, unsigned int screenHeight)
{
    _scene = scene;

    for (Mesh& mesh : _scene->meshes) {
        // Compile shaders
        if (!sc.compileShader(mesh.VS, "bin/shaders/phong.vs", GL_VERTEX_SHADER, errorLog)) {
            std::cerr << "Error compiling shader VS: " << errorLog << std::endl;
            return false;
        }
        if (!sc.compileShader(mesh.FS, "bin/shaders/phong.fs", GL_FRAGMENT_SHADER, errorLog)) {
            std::cerr << "Error compiling shader FS: " << errorLog << std::endl;
            return false;
        }
        // Link shaders
        if (!sc.linkProgram(mesh.shaderProg, errorLog, 2, mesh.VS, mesh.FS)) {
            std::cerr << "Error linking shaders to shaderProg: " << errorLog << std::endl;
            return false;
        }
    }

    //Acquire attribute and uniform locations
    for (Mesh& mesh : _scene->meshes) {
        mesh.PositionAttrib = glGetAttribLocation(mesh.shaderProg, "position");
        if (mesh.PositionAttrib == -1) {
            std::cerr << "position not a valid glsl program variable" << std::endl;
        }
        mesh.NormalAttrib = glGetAttribLocation(mesh.shaderProg, "normal");
        if (mesh.NormalAttrib == -1) {
            std::cerr << "normal not a valid glsl program variable" << std::endl;
        }
        mesh.TextureAttrib = glGetAttribLocation(mesh.shaderProg, "texcoord");
        if (mesh.TextureAttrib == -1) {
            std::cerr << "texcoord not a valid glsl program variable" << std::endl;
        }
        mesh.CamPosUniform = glGetUniformLocation(mesh.shaderProg, "camPos");
        if (mesh.CamPosUniform == -1) {
            std::cerr << "camPos not a valid glsl program variable" << std::endl;
        }
        mesh.VPUniform = glGetUniformLocation(mesh.shaderProg, "vp");
        if (mesh.VPUniform == -1) {
            std::cerr << "vp not a valid glsl program variable" << std::endl;
        }
        mesh.MUniform = glGetUniformLocation(mesh.shaderProg, "m");
        if (mesh.MUniform == -1) {
            std::cerr << "m not a valid glsl program variable" << std::endl;
        }
        mesh.LightPosUniform = glGetUniformLocation(mesh.shaderProg, "lightPos");
        if (mesh.LightPosUniform == -1) {
            std::cerr << "lightPos not a valid glsl program variable" << std::endl;
        }
        mesh.LAUniform = glGetUniformLocation(mesh.shaderProg, "lightAmbientColor");
        if (mesh.LAUniform == -1) {
            std::cerr << "lightAmbientColor not a valid glsl program variable" << std::endl;
        }
        mesh.LDUniform = glGetUniformLocation(mesh.shaderProg, "lightDiffuseColor");
        if (mesh.LDUniform == -1) {
            std::cerr << "lightDiffuseColor not a valid glsl program variable" << std::endl;
        }
        mesh.LSUniform = glGetUniformLocation(mesh.shaderProg, "lightSpecularColor");
        if (mesh.LSUniform == -1) {
            std::cerr << "lightSpecularColor not a valid glsl program variable" << std::endl;
        }
        mesh.AmbientUniform = glGetUniformLocation(mesh.shaderProg, "materialAmbient");
        if (mesh.AmbientUniform == -1) {
            std::cerr << "materialAmbient not a valid glsl program variable" << std::endl;
        }
        mesh.DiffuseUniform = glGetUniformLocation(mesh.shaderProg, "materialDiffuse");
        if (mesh.DiffuseUniform == -1) {
            std::cerr << "materialDiffuse not a valid glsl program variable" << std::endl;
        }
        mesh.SpecularUniform = glGetUniformLocation(mesh.shaderProg, "materialSpecular");
        if (mesh.SpecularUniform == -1) {
            std::cerr << "materialSpecular not a valid glsl program variable" << std::endl;
        }
        mesh.ShininessUniform = glGetUniformLocation(mesh.shaderProg, "materialShininess");
        if (mesh.ShininessUniform == -1) {
            std::cerr << "materialShininess not a valid glsl program variable" << std::endl;
        }
        mesh.diffuseTextureSampler = glGetUniformLocation(mesh.shaderProg, "diffuseTexture");
        if (mesh.diffuseTextureSampler == -1) {
            std::cerr << "diffuseTextureSampler not a valid glsl program variable" << std::endl;
        }
        mesh.shadowMapSampler = glGetUniformLocation(mesh.shaderProg, "shadowMap");
        if (mesh.shadowMapSampler == -1) {
            std::cerr << "shadowMap not a valid glsl program variable" << std::endl;
        }
        mesh.lightSpaceMatrixUniform = glGetUniformLocation(mesh.shaderProg, "lightSpaceMatrix");
        if (mesh.lightSpaceMatrixUniform == -1) {
            std::cerr << "lightSpaceMatrix not a valid glsl program variable" << std::endl;
        }
    }

    // Light
    if (!sc.compileShader(lightVS, "bin/shaders/light.vs", GL_VERTEX_SHADER, errorLog)) {
        std::cerr << "Error compiling light VS: " << errorLog << std::endl;
        return false;
    }
    if (!sc.compileShader(lightFS, "bin/shaders/light.fs", GL_FRAGMENT_SHADER, errorLog)) {
        std::cerr << "Error compiling light FS: " << errorLog << std::endl;
        return false;
    }
    if (!sc.linkProgram(lightProg, errorLog, 2, lightVS, lightFS)) {
        std::cerr << "Error linking shaders to shaderProg: " << errorLog << std::endl;
        return false;
    }

    //Acquire light attribute and uniform locations
    lightMvpUniform = glGetUniformLocation(lightProg, "mvp");
    if (lightMvpUniform == -1) {
        std::cerr << "light mvp not a valid glsl program variable" << std::endl;
    }
    lightAmbientUniform = glGetUniformLocation(lightProg, "lightAmbientColor");
    if (lightAmbientUniform == -1) {
        std::cerr << "lightAmbientColor not a valid glsl program variable" << std::endl;
    }
    lightDiffuseUniform = glGetUniformLocation(lightProg, "lightDiffuseColor");
    if (lightDiffuseUniform == -1) {
        std::cerr << "lightDiffuseColor not a valid glsl program variable" << std::endl;
    }
    lightPosAttrib = glGetAttribLocation(lightProg, "position");
    if (lightPosAttrib == -1) {
        std::cerr << "position not a valid glsl program variable" << std::endl;
    }

    // Shadow
    if (!sc.compileShader(shadowMapVS, "bin/shaders/shadowmap.vs", GL_VERTEX_SHADER, errorLog)) {
        std::cerr << "Error compiling shadowmap VS: " << errorLog << std::endl;
        return false;
    }
    if (!sc.compileShader(shadowMapFS, "bin/shaders/shadowmap.fs", GL_FRAGMENT_SHADER, errorLog)) {
        std::cerr << "Error compiling shadowmap FS: " << errorLog << std::endl;
        return false;
    }
    if (!sc.linkProgram(shadowMapProg, errorLog, 2, shadowMapVS, shadowMapFS)) {
        std::cerr << "Error linking shaders to shadowMapProg: " << errorLog << std::endl;
        return false;
    }

    // Acquire shadow attribute and uniform locations
    depthPositionAttrib = glGetAttribLocation(shadowMapProg, "position");
    if (depthPositionAttrib == -1) {
        std::cerr << "shadow mvp not a valid glsl program variable" << std::endl;
    }
    depthMvpUniform = glGetUniformLocation(shadowMapProg, "mvp");
    if (depthMvpUniform == -1) {
        std::cerr << "shadow mvp not a valid glsl program variable" << std::endl;
    }
    depthModelUniform = glGetUniformLocation(shadowMapProg, "model");
    if (depthModelUniform == -1) {
        std::cerr << "shadowMap model not a valid glsl program variable" << std::endl;
    }

    // Setup Meshes
    for (Mesh& mesh : _scene->meshes) {
        mesh.setupMesh();
        // Setup mesh textures
        Texture& tex = _scene->textures[_scene->materials[mesh.materialIndex].textureIndex];
        glGenTextures(1, &mesh.textureHandler);
        glBindTexture(GL_TEXTURE_2D, mesh.textureHandler);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex.width, tex.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &tex.data);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    const float lightquad[][3] = {
        { -2.0, 0, -0.4 },
        { -2.0, 0, 0.4 },
        { 2.0, 0, 0.4 },
        { 2.0, 0, -0.4 }
    };

    // Setup Light
    glGenVertexArrays(1, &lightVAO);
    glGenBuffers(1, &lightquadVBO);
    glBindVertexArray(lightVAO);
    glBindBuffer(GL_ARRAY_BUFFER, lightquadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lightquad), lightquad, GL_STATIC_DRAW);
    glVertexAttribPointer(lightPosAttrib, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(lightPosAttrib);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Compile shaders
    if (!sc.compileShader(shadowMapViewportVS, "bin/shaders/viewport.vs", GL_VERTEX_SHADER, errorLog)) {
        std::cerr << "Error compiling shadowMapViewport VS: " << errorLog << std::endl;
        return false;
    }
    if (!sc.compileShader(shadowMapViewportFS, "bin/shaders/viewport.fs", GL_FRAGMENT_SHADER, errorLog)) {
        std::cerr << "Error compiling shadowMapViewport FS: " << errorLog << std::endl;
        return false;
    }
    if (!sc.linkProgram(shadowMapViewportProg, errorLog, 2, shadowMapViewportVS, shadowMapViewportFS)) {
        std::cerr << "Error linking shaders to shadowMapViewportProg: " << errorLog << std::endl;
        return false;
    }

    // VIEWPORT ----------------------------------------------------------------------------------------
    static const GLfloat g_quad_vertex_buffer_data[] = {
        -1.0f,
        -1.0f,
        0.0f,
        1.0f,
        -1.0f,
        0.0f,
        -1.0f,
        1.0f,
        0.0f,
        -1.0f,
        1.0f,
        0.0f,
        1.0f,
        -1.0f,
        0.0f,
        1.0f,
        1.0f,
        0.0f,
    };

    glGenVertexArrays(1, &viewportVAO);
    glGenBuffers(1, &quad_vertexbuffer);

    glBindVertexArray(viewportVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    viewportTexture = glGetUniformLocation(shadowMapViewportProg, "tex");
    if (viewportTexture == -1) {
        std::cerr << "tex not a valid glsl program variable" << std::endl;
    }

    // ---------------------------------------------------------------------------------------------------

    // SHADOW MAP INIT
    // Create the FBO
    glGenFramebuffers(1, &depthMapFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);

    // Create the depth texture
    glGenTextures(1, &depthMapTex);
    glBindTexture(GL_TEXTURE_2D, depthMapTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthMapTex, 0);

    // Disable writes to the color buffer
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // check that framebuffer is ok
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        return false;

    // UTILS
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Clear color
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    return true;
}

void StudentRenderer::onUpdate(float timeSinceLastUpdateMs)
{
    // Light update
    lightPos.y = _scene->bbox.maxPoint.y + 6;
    lightPos.z = _scene->bbox.maxPoint.z - 3;
    lightPos.x = -sin((SDL_GetTicks() / 500.0f) / 3.0f) * 2.0f;
    //lightPos.z = sin((SDL_GetTicks() / 100.0f) / 3.0f) * 2.0f;

    // Earth update
    if(earthPos.y <= _scene->bbox.minPoint.y-2){
        earthDir = true;
    }else if(earthPos.y > -0.05){
        earthDir = false;
    }

    if(earthDir){
        earthPos += glm::vec3(0, 0.02, 0);
    }else{
        earthPos -= glm::vec3(0, 0.02, 0);
    }
}

void StudentRenderer::onKeyPressed(SDL_Keycode code)
{
}

void StudentRenderer::onWindowRedraw(OrbitalCamera Camera, unsigned int screenWidth, unsigned int screenHeight)
{

    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glViewport(0, 0, 1024, 1024);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Prepare render space for depth
    glm::mat4 lightProjection, lightView;
    glm::mat4 lightSpaceMatrix;
    //lightProjection = glm::ortho<float>(-10, 10, -10, 10, 2.0f, 10.0f);
    //lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
    lightProjection = glm::perspective<float>(45.0f, 1.0f, 1.0f, 25.0f);
    lightView = glm::lookAt(lightPos, glm::vec3(0.0, 0.0, 0.0) - lightPos, glm::vec3(0, 1, 0));
    lightSpaceMatrix = lightProjection * lightView;

    // render scene from light's point of view
    glUseProgram(shadowMapProg);
    glUniformMatrix4fv(depthMvpUniform, 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

    // Draw meshes
    for (int i = 0; i < _scene->meshes.size(); ++i) {
        // Draw all but not the plane so the shadow of the plane don't project
        //if (i != 0) {
            Mesh& mesh = _scene->meshes[i];
            if (i == 46) {
                mesh.modelMatrix = glm::translate(earthPos);
            } else {
                mesh.modelMatrix = glm::translate(glm::vec3(0, 0, 0));
            }

            glBindVertexArray(mesh.VAOtest);
            glUniformMatrix4fv(depthModelUniform, 1, GL_FALSE, glm::value_ptr(mesh.modelMatrix));

            glDrawElements(
                GL_TRIANGLES, // mode
                mesh.indices.size(), // count
                GL_UNSIGNED_INT, // type
                (void*)0 // element array buffer offset
            );
            glBindVertexArray(0);
        //}
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // ------------------------------------------------------------------------------------

    // ----------------------------------------------------------------------------------------
    // DRAW NORMAL WITH DEPTH
    glViewport(0, 0, screenWidth, screenHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);

    // Draw light
    glUseProgram(lightProg);
    glm::mat4 lightRepresentationMVP = Camera.getViewProjectionMatrix() * glm::translate(lightPos);
    glUniformMatrix4fv(lightMvpUniform, 1, GL_FALSE, glm::value_ptr(lightRepresentationMVP));
    glUniform3fv(lightDiffuseUniform, 1, glm::value_ptr(glm::vec3(0.2, 0.2, 0.2)));

    glBindVertexArray(lightVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 5);
    glBindVertexArray(0);

    // Draw meshes
    int i = 1;
    for (Mesh& mesh : _scene->meshes) {
        glUseProgram(mesh.shaderProg);

        glBindVertexArray(mesh.VAO);

        // Load shadowMap Uniforms
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, depthMapTex);
        glUniform1i(mesh.shadowMapSampler, 0);
        glUniformMatrix4fv(mesh.lightSpaceMatrixUniform, 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

        if (_scene->materials[mesh.materialIndex].textureIndex >= 0) {
            glActiveTexture(GL_TEXTURE0 + i); // activate proper texture unit before binding
            glUniform1i(mesh.diffuseTextureSampler, i);
            ++i;
            glBindTexture(GL_TEXTURE_2D, mesh.textureHandler);
        }

        // Load uniforms
        glUniform3fv(mesh.CamPosUniform, 1, glm::value_ptr(Camera.getPosition()));
        glUniformMatrix4fv(mesh.VPUniform, 1, GL_FALSE, glm::value_ptr(Camera.getViewProjectionMatrix()));
        glUniform3fv(mesh.LAUniform, 1, glm::value_ptr(lightAmbientColor));
        glUniform3fv(mesh.LDUniform, 1, glm::value_ptr(lightDiffuseColor));
        glUniform3fv(mesh.LSUniform, 1, glm::value_ptr(lightSpecularColor));

        glUniform3fv(mesh.AmbientUniform, 1, glm::value_ptr(_scene->materials[mesh.materialIndex].ambient));
        glUniform3fv(mesh.DiffuseUniform, 1, glm::value_ptr(_scene->materials[mesh.materialIndex].diffuse));
        glUniform3fv(mesh.AmbientUniform, 1, glm::value_ptr(_scene->materials[mesh.materialIndex].specular));
        glUniform1fv(mesh.ShininessUniform, 1, &_scene->materials[mesh.materialIndex].shininess);

        if (i == 28) {
            mesh.modelMatrix = glm::translate(earthPos);
        } else {
            mesh.modelMatrix = glm::translate(glm::vec3(0, 0, 0));
        }

        glUniformMatrix4fv(mesh.MUniform, 1, GL_FALSE, glm::value_ptr(mesh.modelMatrix));
        glUniform3fv(mesh.LightPosUniform, 1, glm::value_ptr(lightPos));

        glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, NULL);

        // RESETS
        // Unbind VAO
        glBindVertexArray(0);
    }

    // ----------------------------------------------------------------------------------------

    // Render only on a corner of the window (or we we won't see the real rendering...)
    glViewport(0, 0, 512, 512);

    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Use our shader
    glUseProgram(shadowMapViewportProg);

    // Bind our texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthMapTex);

    // 1rst attribute buffer : vertices
    glBindVertexArray(viewportVAO);
    // Set our "renderedTexture" sampler to use Texture Unit 0
    glUniform1i(viewportTexture, 0);
    // Draw the triangle !
    // You have to disable GL_COMPARE_R_TO_TEXTURE above in order to see anything !
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void StudentRenderer::clearStudentData()
{
}