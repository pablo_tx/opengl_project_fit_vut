#pragma once

#include <SDL.h>
#include <GL/glew.h>
#include <string>

#include "Scene.hpp"
#include "ShaderCompiler.hpp"
#include "HighResolutionTimer.hpp"
#include "CameraPath.h"
#include "OrbitalCamera.hpp"

/*
* File:   StudentRenderer.cpp
* Author: Pablo Pozo Tena
* Date:
*/

class StudentRenderer
{
public:
	StudentRenderer();

	bool init(std::shared_ptr<Scene> scene, unsigned int screenWidth, unsigned int screenHeight);
	void onUpdate(float timeSinceLastUpdateMs);
	void onWindowRedraw(OrbitalCamera Camera, unsigned int screenWidth, unsigned int screenHeight);
	void onKeyPressed(SDL_Keycode code);
	void clearStudentData();

private:
	void drawScene(glm::mat4 cameraViewProjectionMatrix, glm::vec3 cameraPosition);
	std::shared_ptr<Scene> _scene;
	
	ShaderCompiler sc;
	std::string errorLog;

	//Earth
	glm::vec3 earthPos = glm::vec3(0, -2, 0);
	bool earthDir = true;
	
	//Light
	GLuint lightquadVBO, lightVAO;
	GLuint lightVS, lightFS, lightProg;
	GLuint lightMvpUniform, lightPosAttrib, lightDiffuseUniform, lightAmbientUniform;
	//Light pos
	glm::vec3 lightPos = glm::vec3(0, 0, 0);
	glm::vec3 lightInvDir = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::mat4 lightModelMatrix = glm::mat4(1.0f);
	//Light color properties
	glm::vec3 lightAmbientColor = glm::vec3(0.5, 0.6, 0.5);
	glm::vec3 lightDiffuseColor =  glm::vec3(0.8, 1, 0.8);
	glm::vec3 lightSpecularColor = glm::vec3(0.2, 0.7, 0.2);

	// Shadow map
	GLuint depthMapTex;
	GLuint depthMapFBO;
	GLuint shadowMapVS, shadowMapFS, shadowMapProg;

	//Plane/Depth shadow map
	GLuint planeVBO, planeVAO;
	GLuint planePositionAttrib, planeNormalAttrib, planeTexAttrib;
	GLuint depthModelUniform;
	GLuint depthMvpUniform;
	GLuint depthPositionAttrib;

	// ShadowMap Viewport
	GLuint shadowMapViewportProg, shadowMapViewportVS, shadowMapViewportFS, viewportVAO;
	GLuint viewportTexture, quad_vertexbuffer, viewportVertexPositionAttrib;
};
