#pragma once

#include <limits>
#include <memory>
#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include "glm/gtc/matrix_transform.hpp"
#include <GL/glew.h>

//Axis-aligned bounding box
struct AABB {
    glm::vec3 minPoint;
    glm::vec3 maxPoint;

    AABB()
    {
        //Initialize to +/- infinity
        float maxFloat = std::numeric_limits<float>::infinity();
        float minFloat = -std::numeric_limits<float>::infinity();

        minPoint = glm::vec3(maxFloat, maxFloat, maxFloat);
        maxPoint = glm::vec3(minFloat, minFloat, minFloat);
    }

    void updateWithTriangle(const glm::vec3* triangleVertices)
    {
        updateWithVertex(triangleVertices[0]);
        updateWithVertex(triangleVertices[1]);
        updateWithVertex(triangleVertices[2]);
    }

    void updateWithVertex(const glm::vec3& vertex)
    {
        if (vertex.x < minPoint.x)
            minPoint.x = vertex.x;

        if (vertex.y < minPoint.y)
            minPoint.y = vertex.y;

        if (vertex.z < minPoint.z)
            minPoint.z = vertex.z;

        if (vertex.x > maxPoint.x)
            maxPoint.x = vertex.x;

        if (vertex.y > maxPoint.y)
            maxPoint.y = vertex.y;

        if (vertex.z > maxPoint.z)
            maxPoint.z = vertex.z;
    }

    void updateWithVertex(const glm::vec4& vertex)
    {
        updateWithVertex(glm::vec3(vertex));
    }

    void updateWithAABB(const AABB& bbox)
    {
        updateWithVertex(bbox.minPoint);
        updateWithVertex(bbox.maxPoint);
    }

    AABB getTransformedAABB(const glm::mat4 matrix)
    {
        AABB newBB;
        newBB.minPoint = glm::vec3(matrix * glm::vec4(minPoint, 1.0f));
        newBB.maxPoint = glm::vec3(matrix * glm::vec4(maxPoint, 1.0f));

        return newBB;
    }
};

struct Material {
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    float shininess;

    //Index to a texture array
    //If negative - material has no texture
    int textureIndex;

    //Alignment to 128bit, in terms of size
    float aligmetVariableDoNotUse1, aligmetVariableDoNotUse2;
};

struct Mesh {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<unsigned int> indices;
    std::vector<glm::vec2> texcoords;

    AABB bbox;

    glm::mat4 modelMatrix;
    unsigned int materialIndex;

    GLuint VAO, VBO, EBO, VBOtest, VAOtest, EBOtest;

    //Attribs and Uniform locations
	GLuint PositionAttrib, VPUniform, ColorUniform, NormalAttrib, MUniform, NUniform, CamPosUniform, TextureAttrib, diffuseTextureSampler;
	GLuint LightPosUniform, LAUniform, LDUniform, LSUniform;
    GLuint SpecularUniform, DiffuseUniform, AmbientUniform, ShininessUniform;
    GLuint lightSpaceMatrixUniform;

    // Shaders
    GLuint VS, FS, shaderProg;
    
    // Tex
    GLuint textureHandler;

    //
    GLuint shadowMapSampler;
    

    void setupMesh(){
        // Generate buffers and VA
        glGenVertexArrays( 1, &this->VAO);
        glGenVertexArrays( 1, &this->VAOtest);
        glGenBuffers(1, &this->VBO);
        glGenBuffers(1, &this->EBO);
        glGenBuffers(1, &this->EBOtest);
        glGenBuffers(1, &this->VBOtest);

        // Create full VAO
        glBindVertexArray( VAO );

        //Generate VBO
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, (sizeof(glm::vec3) * (vertices.size() + normals.size())) + (sizeof(glm::vec2) * texcoords.size()), NULL, GL_STATIC_DRAW);

        // VBO SUBDATA
        glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(glm::vec3), vertices.data());
        glBufferSubData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), normals.size() * sizeof(glm::vec3), normals.data());
        glBufferSubData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * (vertices.size() + normals.size()), texcoords.size() * sizeof(glm::vec2), texcoords.data());
        
        //Generate EBO
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

        // Load VertexAttribs
        glVertexAttribPointer(PositionAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
        glEnableVertexAttribArray(PositionAttrib);
        glVertexAttribPointer(NormalAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(glm::vec3) * vertices.size()));
        glEnableVertexAttribArray(NormalAttrib);
        glVertexAttribPointer(TextureAttrib, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(glm::vec3) * (vertices.size() + normals.size())));
        glEnableVertexAttribArray(TextureAttrib);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray( 0 );

        //Generate simplified VAO
        glBindVertexArray( VAOtest );

        //Generate VBO
        glBindBuffer(GL_ARRAY_BUFFER, VBOtest);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
        
        //Generate EBO
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOtest);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

        // Load VertexAttribs
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray( 0 );
    }
};

//Texture
//Always R8G8B8A8 format
struct Texture {
    Texture()
    {
        width = height = 0;
        data = nullptr;
    }

    unsigned int width;
    unsigned int height;

    std::shared_ptr<unsigned char> data;
};

struct Scene {
    //This is the data that interests You
    std::vector<Mesh> meshes;

    std::vector<Material> materials;
    std::vector<Texture> textures;

    AABB bbox;
};
