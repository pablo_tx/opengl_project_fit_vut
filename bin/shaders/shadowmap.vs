#version 330

layout (location = 0) in vec3 position;

uniform mat4 mvp;
uniform mat4 model;

void main()
{
    gl_Position = mvp * model * vec4(position, 1.0);
}