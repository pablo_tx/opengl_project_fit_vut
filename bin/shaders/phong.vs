#version 330

// Inputs
layout (location = 0) in vec3 position; // Vertex position
layout (location = 1) in vec3 normal; // Vertex normal
layout (location = 2) in vec2 texcoord; // Vertex texture coordinates

// View Projection matrix
uniform mat4 vp;

// Model matrix
uniform mat4 m;

// Normal transformation matrix (transposed inverse of model matrix)
//uniform mat4 n;

uniform mat4 lightSpaceMatrix;

// TODO - Define output variables that are necessary to calculate
//        lighting in fragment shader 
//        (hint: you will need normal and worldspace position)
out vec3 normalOut;
out vec4 worldPos;
out vec2 texcoordOut;
out vec4 lightSpaceMatrixOut;

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

void main()
{
    gl_Position = vp * m * vec4(position, 1);

    // TODO  - Pass on some transformed vectors to fragment shader.
    //         When transforming normal, the weight (w component) should be set to 0 when
    //         transforming it with a 4x4 matrix, otherwise it will also translate, which is unwanted.
    //         Also, normal vector has to be transformed by inversed transpose of model matrix, due to
    //         possible scaling factor in model matrix.
    //         (this matrix has been prepared for Your convenience - the "n" uniform variable)
    //         You will also need something else apart from the normal.

	worldPos = m * vec4(position, 1);
	normalOut = (transpose(inverse(mat3(m))) * vec3(normal)).xyz;
    texcoordOut = texcoord;
    lightSpaceMatrixOut = lightSpaceMatrix * vec4(position,1);
}
