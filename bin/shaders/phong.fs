#version 330 core

// TODO - Define input variables from vertex shader necessary to compute lighting
in vec3 normalOut;
in vec4 worldPos;
in vec2 texcoordOut;
in vec4 lightSpaceMatrixOut;

// Camera position in world space
uniform vec3 camPos;

// Light
uniform vec3 lightPos;          // Light position in world space
uniform vec3 lightAmbientColor; // Light color for mixing with material ambient color
uniform vec3 lightDiffuseColor; // Light color for mixing with material diffuse color
uniform vec3 lightSpecularColor;// Light color for mixing with material specular color

// Material colors
uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float materialShininess;

uniform sampler2D shadowMap;
uniform sampler2D diffuseTexture;


// Output fragment color to framebuffer
out vec4 fragColor; 



///////////////////////////////

float ShadowCalculation(vec4 lightSpaceMatrixOut)
{
    // perform perspective divide
    vec3 projCoords = lightSpaceMatrixOut.xyz / lightSpaceMatrixOut.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow

    // FIX for shadow acne
    float bias = 0.005;
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;  
    //float shadow = currentDepth > closestDepth  ? 1.0 : 0.0;

    return shadow;
} 
//////////////////////////////

void main()
{
    // Color components of Phong lighting model
    vec3 ambient = vec3(0, 0, 0);
    vec3 diffuse = vec3(0, 0, 0);
    vec3 specular = vec3(0, 0, 0);
    vec3 color = texture(diffuseTexture, texcoordOut).rgb;

    // TODO - Prepare data from VS
    // Normalize what is necessary
	vec4 worldPosR= worldPos / worldPos.w;

	vec3 N = normalize(normalOut);
	vec3 L = normalize(lightPos-worldPosR.xyz);
	vec3 R = normalize(reflect(-L, N));
	vec3 V = normalize(camPos-worldPosR.xyz);

    // TODO - Calculate ambient lighting
	float ambientFactor = 0.5;
    ambient = ambientFactor * lightAmbientColor * materialAmbient;

    // TODO - Calculate diffuse and specular lighting
    float diffuseFactor = 0.7;
    if (diffuseFactor > 0) {
        diffuse = diffuseFactor * max(0, dot(N, L))* lightDiffuseColor * materialDiffuse;
    
        // TODO - Calculate specular lighting
        // Don't forget to normalize vectors (not positions) used in lighting model calculations
        float specularFactor = 0.3;
        if (specularFactor > 0) {
            specular = specularFactor * pow(max(0, dot(V, R)), materialShininess) * lightSpecularColor * materialSpecular;
        }
    }

    //float visibility = texture( shadowMap, vec3(lightSpaceMatrixOut.xy, (lightSpaceMatrixOut.z)/lightSpaceMatrixOut.w) );
    float shadow = ShadowCalculation(lightSpaceMatrixOut); 

    //vec3 lighting = (ambient + (1.0 - shadow)) * (diffuse + specular) * color;
    vec3 lighting = (ambient + (1.0 - shadow)) * (diffuse + specular);

    //Writing color to the output
    fragColor = vec4(lighting, 1);
}
