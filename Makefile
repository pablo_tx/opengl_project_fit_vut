# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/pablo/sync/Curso/Graphics/Proyecto

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/pablo/sync/Curso/Graphics/Proyecto

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/ccmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles /home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named ComputerGraphicsProject

# Build rule for target.
ComputerGraphicsProject: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 ComputerGraphicsProject
.PHONY : ComputerGraphicsProject

# fast build rule for target.
ComputerGraphicsProject/fast:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/build
.PHONY : ComputerGraphicsProject/fast

src/Application.o: src/Application.cpp.o

.PHONY : src/Application.o

# target to build an object file
src/Application.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/Application.cpp.o
.PHONY : src/Application.cpp.o

src/Application.i: src/Application.cpp.i

.PHONY : src/Application.i

# target to preprocess a source file
src/Application.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/Application.cpp.i
.PHONY : src/Application.cpp.i

src/Application.s: src/Application.cpp.s

.PHONY : src/Application.s

# target to generate assembly for a file
src/Application.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/Application.cpp.s
.PHONY : src/Application.cpp.s

src/CameraPath.o: src/CameraPath.cpp.o

.PHONY : src/CameraPath.o

# target to build an object file
src/CameraPath.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/CameraPath.cpp.o
.PHONY : src/CameraPath.cpp.o

src/CameraPath.i: src/CameraPath.cpp.i

.PHONY : src/CameraPath.i

# target to preprocess a source file
src/CameraPath.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/CameraPath.cpp.i
.PHONY : src/CameraPath.cpp.i

src/CameraPath.s: src/CameraPath.cpp.s

.PHONY : src/CameraPath.s

# target to generate assembly for a file
src/CameraPath.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/CameraPath.cpp.s
.PHONY : src/CameraPath.cpp.s

src/FreelookCamera.o: src/FreelookCamera.cpp.o

.PHONY : src/FreelookCamera.o

# target to build an object file
src/FreelookCamera.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/FreelookCamera.cpp.o
.PHONY : src/FreelookCamera.cpp.o

src/FreelookCamera.i: src/FreelookCamera.cpp.i

.PHONY : src/FreelookCamera.i

# target to preprocess a source file
src/FreelookCamera.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/FreelookCamera.cpp.i
.PHONY : src/FreelookCamera.cpp.i

src/FreelookCamera.s: src/FreelookCamera.cpp.s

.PHONY : src/FreelookCamera.s

# target to generate assembly for a file
src/FreelookCamera.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/FreelookCamera.cpp.s
.PHONY : src/FreelookCamera.cpp.s

src/HighResolutionTimer.o: src/HighResolutionTimer.cpp.o

.PHONY : src/HighResolutionTimer.o

# target to build an object file
src/HighResolutionTimer.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/HighResolutionTimer.cpp.o
.PHONY : src/HighResolutionTimer.cpp.o

src/HighResolutionTimer.i: src/HighResolutionTimer.cpp.i

.PHONY : src/HighResolutionTimer.i

# target to preprocess a source file
src/HighResolutionTimer.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/HighResolutionTimer.cpp.i
.PHONY : src/HighResolutionTimer.cpp.i

src/HighResolutionTimer.s: src/HighResolutionTimer.cpp.s

.PHONY : src/HighResolutionTimer.s

# target to generate assembly for a file
src/HighResolutionTimer.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/HighResolutionTimer.cpp.s
.PHONY : src/HighResolutionTimer.cpp.s

src/ModelLoader.o: src/ModelLoader.cpp.o

.PHONY : src/ModelLoader.o

# target to build an object file
src/ModelLoader.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/ModelLoader.cpp.o
.PHONY : src/ModelLoader.cpp.o

src/ModelLoader.i: src/ModelLoader.cpp.i

.PHONY : src/ModelLoader.i

# target to preprocess a source file
src/ModelLoader.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/ModelLoader.cpp.i
.PHONY : src/ModelLoader.cpp.i

src/ModelLoader.s: src/ModelLoader.cpp.s

.PHONY : src/ModelLoader.s

# target to generate assembly for a file
src/ModelLoader.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/ModelLoader.cpp.s
.PHONY : src/ModelLoader.cpp.s

src/OrbitalCamera.o: src/OrbitalCamera.cpp.o

.PHONY : src/OrbitalCamera.o

# target to build an object file
src/OrbitalCamera.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/OrbitalCamera.cpp.o
.PHONY : src/OrbitalCamera.cpp.o

src/OrbitalCamera.i: src/OrbitalCamera.cpp.i

.PHONY : src/OrbitalCamera.i

# target to preprocess a source file
src/OrbitalCamera.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/OrbitalCamera.cpp.i
.PHONY : src/OrbitalCamera.cpp.i

src/OrbitalCamera.s: src/OrbitalCamera.cpp.s

.PHONY : src/OrbitalCamera.s

# target to generate assembly for a file
src/OrbitalCamera.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/OrbitalCamera.cpp.s
.PHONY : src/OrbitalCamera.cpp.s

src/SceneLoader.o: src/SceneLoader.cpp.o

.PHONY : src/SceneLoader.o

# target to build an object file
src/SceneLoader.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/SceneLoader.cpp.o
.PHONY : src/SceneLoader.cpp.o

src/SceneLoader.i: src/SceneLoader.cpp.i

.PHONY : src/SceneLoader.i

# target to preprocess a source file
src/SceneLoader.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/SceneLoader.cpp.i
.PHONY : src/SceneLoader.cpp.i

src/SceneLoader.s: src/SceneLoader.cpp.s

.PHONY : src/SceneLoader.s

# target to generate assembly for a file
src/SceneLoader.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/SceneLoader.cpp.s
.PHONY : src/SceneLoader.cpp.s

src/ShaderCompiler.o: src/ShaderCompiler.cpp.o

.PHONY : src/ShaderCompiler.o

# target to build an object file
src/ShaderCompiler.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/ShaderCompiler.cpp.o
.PHONY : src/ShaderCompiler.cpp.o

src/ShaderCompiler.i: src/ShaderCompiler.cpp.i

.PHONY : src/ShaderCompiler.i

# target to preprocess a source file
src/ShaderCompiler.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/ShaderCompiler.cpp.i
.PHONY : src/ShaderCompiler.cpp.i

src/ShaderCompiler.s: src/ShaderCompiler.cpp.s

.PHONY : src/ShaderCompiler.s

# target to generate assembly for a file
src/ShaderCompiler.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/ShaderCompiler.cpp.s
.PHONY : src/ShaderCompiler.cpp.s

src/StudentRenderer.o: src/StudentRenderer.cpp.o

.PHONY : src/StudentRenderer.o

# target to build an object file
src/StudentRenderer.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/StudentRenderer.cpp.o
.PHONY : src/StudentRenderer.cpp.o

src/StudentRenderer.i: src/StudentRenderer.cpp.i

.PHONY : src/StudentRenderer.i

# target to preprocess a source file
src/StudentRenderer.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/StudentRenderer.cpp.i
.PHONY : src/StudentRenderer.cpp.i

src/StudentRenderer.s: src/StudentRenderer.cpp.s

.PHONY : src/StudentRenderer.s

# target to generate assembly for a file
src/StudentRenderer.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/StudentRenderer.cpp.s
.PHONY : src/StudentRenderer.cpp.s

src/TextureLoader.o: src/TextureLoader.cpp.o

.PHONY : src/TextureLoader.o

# target to build an object file
src/TextureLoader.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/TextureLoader.cpp.o
.PHONY : src/TextureLoader.cpp.o

src/TextureLoader.i: src/TextureLoader.cpp.i

.PHONY : src/TextureLoader.i

# target to preprocess a source file
src/TextureLoader.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/TextureLoader.cpp.i
.PHONY : src/TextureLoader.cpp.i

src/TextureLoader.s: src/TextureLoader.cpp.s

.PHONY : src/TextureLoader.s

# target to generate assembly for a file
src/TextureLoader.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/TextureLoader.cpp.s
.PHONY : src/TextureLoader.cpp.s

src/main.o: src/main.cpp.o

.PHONY : src/main.o

# target to build an object file
src/main.cpp.o:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/main.cpp.o
.PHONY : src/main.cpp.o

src/main.i: src/main.cpp.i

.PHONY : src/main.i

# target to preprocess a source file
src/main.cpp.i:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/main.cpp.i
.PHONY : src/main.cpp.i

src/main.s: src/main.cpp.s

.PHONY : src/main.s

# target to generate assembly for a file
src/main.cpp.s:
	$(MAKE) -f CMakeFiles/ComputerGraphicsProject.dir/build.make CMakeFiles/ComputerGraphicsProject.dir/src/main.cpp.s
.PHONY : src/main.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... rebuild_cache"
	@echo "... ComputerGraphicsProject"
	@echo "... edit_cache"
	@echo "... src/Application.o"
	@echo "... src/Application.i"
	@echo "... src/Application.s"
	@echo "... src/CameraPath.o"
	@echo "... src/CameraPath.i"
	@echo "... src/CameraPath.s"
	@echo "... src/FreelookCamera.o"
	@echo "... src/FreelookCamera.i"
	@echo "... src/FreelookCamera.s"
	@echo "... src/HighResolutionTimer.o"
	@echo "... src/HighResolutionTimer.i"
	@echo "... src/HighResolutionTimer.s"
	@echo "... src/ModelLoader.o"
	@echo "... src/ModelLoader.i"
	@echo "... src/ModelLoader.s"
	@echo "... src/OrbitalCamera.o"
	@echo "... src/OrbitalCamera.i"
	@echo "... src/OrbitalCamera.s"
	@echo "... src/SceneLoader.o"
	@echo "... src/SceneLoader.i"
	@echo "... src/SceneLoader.s"
	@echo "... src/ShaderCompiler.o"
	@echo "... src/ShaderCompiler.i"
	@echo "... src/ShaderCompiler.s"
	@echo "... src/StudentRenderer.o"
	@echo "... src/StudentRenderer.i"
	@echo "... src/StudentRenderer.s"
	@echo "... src/TextureLoader.o"
	@echo "... src/TextureLoader.i"
	@echo "... src/TextureLoader.s"
	@echo "... src/main.o"
	@echo "... src/main.i"
	@echo "... src/main.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

