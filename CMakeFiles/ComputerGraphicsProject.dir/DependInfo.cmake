# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/Application.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/Application.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/CameraPath.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/CameraPath.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/FreelookCamera.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/FreelookCamera.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/HighResolutionTimer.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/HighResolutionTimer.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/ModelLoader.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/ModelLoader.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/OrbitalCamera.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/OrbitalCamera.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/SceneLoader.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/SceneLoader.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/ShaderCompiler.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/ShaderCompiler.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/StudentRenderer.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/StudentRenderer.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/TextureLoader.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/TextureLoader.cpp.o"
  "/home/pablo/sync/Curso/Graphics/Proyecto/src/main.cpp" "/home/pablo/sync/Curso/Graphics/Proyecto/CMakeFiles/ComputerGraphicsProject.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "ComputerGraphicsProject"
  "PUBLIC"
  "/usr/include/SDL2"
  "/usr/include/IL"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
